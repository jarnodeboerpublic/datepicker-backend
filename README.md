# datepicker-backend
A datepicker backend that is written in Java Spring Boot.

includes:
    - Docker
    - Kubernetes
    - JPA = PostgreSQL
    - Messaging through AWS


//package datepicker.service;
//
//import com.amazonaws.auth.AWSCredentialsProvider;
//import com.amazonaws.auth.AWSStaticCredentialsProvider;
//import com.amazonaws.auth.BasicAWSCredentials;
//import com.amazonaws.services.sns.AmazonSNS;
//import com.amazonaws.services.sns.AmazonSNSClientBuilder;
//import com.amazonaws.services.sns.model.PublishResult;
//import javax.annotation.PostConstruct;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//@Slf4j
//@Component
//public class NotificationService {
//  @Value("${sns.topic.arn}")
//  private String snsTopicARN;
//
//  @Value("${aws.accessKey}")
//  private String awsAccessKey;
//
//  @Value("${aws.secretKey}")
//  private String awsSecretKey;
//
//  @Value("${aws.region}")
//  private String awsRegion;
//
//  private AmazonSNS amazonSNS;
//
//  @PostConstruct
//  private void postConstructor() {
//
//    log.info("SQS URL: " + snsTopicARN);
//
//    AWSCredentialsProvider awsCredentialsProvider =
//        new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey));
//
//    this.amazonSNS =
//        AmazonSNSClientBuilder.standard()
//            .withCredentials(awsCredentialsProvider)
//            .withRegion(awsRegion)
//            .build();
//  }
//
//  public void publishSNSMessage(String message, String subject) {
//
//    log.info("Publishing SNS message: " + message);
//
//    PublishResult result = this.amazonSNS.publish(this.snsTopicARN, message, subject);
//
//    log.info("SNS Message ID: " + result.getMessageId());
//  }
//}

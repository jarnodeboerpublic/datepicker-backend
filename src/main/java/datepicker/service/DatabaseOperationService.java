package datepicker.service;

import datepicker.model.DateCandidates;
import datepicker.model.Datepicker;
import datepicker.repositories.DatepickerRepository;
import datepicker.repositories.ReactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Slf4j
@Service
public class DatabaseOperationService {

  @Autowired private DatepickerRepository datepickerRepository;

  @Autowired private ReactionRepository reactionRepository;

//  @Autowired private NotificationService sns;

  public Datepicker saveDatepicker(Datepicker datepicker, String groupID) {
    log.debug("Saving datepicker to DB");
    datepicker.setNewDatepicker(false);
    datepicker.setGroupID(groupID);
    datepicker.setDateOfChoice("Not yet available");

//    String message = "A new datepicker has been set: " + datepicker.getName();

//    sns.publishSNSMessage("A Datepicker was made for group: " + groupID, message);

    return datepickerRepository.saveAndFlush(datepicker);
  }

  public Datepicker updateDatepicker(Datepicker datepicker, String groupID) {
    Datepicker initDatepicker = datepickerRepository.getOne(datepicker.getDatepickerID());

    if (initDatepicker.getGroupID().equals(groupID)) {
      BeanUtils.copyProperties(
          datepicker, initDatepicker, "groupID", "datepickerID", "dateCandidates");

      Set<DateCandidates> candidates = initDatepicker.getDateCandidates();

      candidates.forEach(
          can -> {
            if (!datepicker.getDateCandidates().contains(can)) {
              reactionRepository.deleteAll(can.getReactions());
            }
          });

      candidates.clear();
      candidates.addAll(datepicker.getDateCandidates());

      return datepickerRepository.saveAndFlush(initDatepicker);
    }
    return null;
  }

  public Datepicker cancelDatepicker(Long id, String groupID) {
    Datepicker datepicker = datepickerRepository.getOne(id);

    if (datepicker.getGroupID().equals(groupID)) {
      datepicker.setCancelled(true);

      return datepickerRepository.saveAndFlush(datepicker);
    }
    return null;
  }

  public Datepicker pickDate(Long id, DateCandidates candidates, String groupID) {
    Datepicker datepicker = datepickerRepository.getOne(id);

    if (datepicker.getGroupID().equals(groupID) && candidates.getDateCandidateId() != null) {
      datepicker.setDateOfChoice(candidates.toString());
      datepicker.setDateCandidateOfChoiceId(candidates.getDateCandidateId());
      datepicker.setClosed(true);

      return datepickerRepository.saveAndFlush(datepicker);
    }
    return null;
  }
}

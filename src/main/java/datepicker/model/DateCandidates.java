package datepicker.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@Entity
@Table(name = "datecandidates")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DateCandidates {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long dateCandidateId;

  private String year;
  private String month;
  private String week;
  private String day;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "dateCandidateId")
  private Set<Reaction> reactions;

  @Override
  public String toString() {
    return day + "-" + month + "-" + year;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DateCandidates that = (DateCandidates) o;
    return Objects.equals(dateCandidateId, that.dateCandidateId)
        && Objects.equals(year, that.year)
        && Objects.equals(month, that.month)
        && Objects.equals(day, that.day);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dateCandidateId, year, month, day);
  }
}

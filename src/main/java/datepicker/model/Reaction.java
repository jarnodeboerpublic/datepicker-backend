package datepicker.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@Entity
@Table(name = "reaction")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Reaction {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long reactionId;

  private String memberID;
  private boolean refusedDate;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Reaction reaction = (Reaction) o;
    return Objects.equals(reactionId, reaction.reactionId)
        && Objects.equals(memberID, reaction.memberID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(reactionId, memberID);
  }
}

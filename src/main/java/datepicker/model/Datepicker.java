package datepicker.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "datepicker")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Datepicker {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long datepickerID;

  @NotNull private String name;
  @NotNull private Boolean newDatepicker;
  @NotNull private String dateOfChoice;
  private Long dateCandidateOfChoiceId;
  @NotNull private Boolean closed;
  @NotNull private Boolean cancelled;
  @NotNull private String groupID;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "datepickerid")
  private Set<DateCandidates> dateCandidates;

  @Override
  public String toString() {
    return "Datepicker{"
        + "id="
        + datepickerID
        + ", name='"
        + name
        + '\''
        + ", newDatepicker="
        + newDatepicker
        + ", dateOfChoice='"
        + dateOfChoice
        + '\''
        + ", closed="
        + closed
        + ", cancelled="
        + cancelled
        + ", groupID='"
        + groupID
        + '\''
        + ", dates="
        + dateCandidatePrint(dateCandidates)
        + '}';
  }

  public String dateCandidatePrint(Set<DateCandidates> candidates) {
    String result = "";
    for (DateCandidates c : candidates) {
      result +=
          "DateCandidates{"
              + "id="
              + c.getDateCandidateId()
              + ", year='"
              + c.getYear()
              + '\''
              + ", month='"
              + c.getMonth()
              + '\''
              + ", week='"
              + c.getWeek()
              + '\''
              + ", day='"
              + c.getDay()
              + '\''
              + ", memberID='"
              + c.getReactions().toString()
              + '\''
              + ", refusedDate="
              + c.getReactions().toString()
              + '}';
    }

    return result;
  }
}

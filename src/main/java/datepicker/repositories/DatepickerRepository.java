package datepicker.repositories;

import datepicker.model.Datepicker;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DatepickerRepository extends JpaRepository<Datepicker, Long> {

  Datepicker getDatepickerByGroupIDAndDatepickerID(@NotNull String groupID, Long datepickerID);

  List<Datepicker> getAllByGroupID(@NotNull String groupID);
}

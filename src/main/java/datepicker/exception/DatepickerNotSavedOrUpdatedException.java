package datepicker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DatepickerNotSavedOrUpdatedException extends RuntimeException {
  public DatepickerNotSavedOrUpdatedException(String message) {
    super(message);
  }
}

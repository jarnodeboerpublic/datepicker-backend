package datepicker.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DatepickerNotFoundException extends RuntimeException {
  public DatepickerNotFoundException(String message) {
    super(message);
  }
}

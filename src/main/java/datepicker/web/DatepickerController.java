package datepicker.web;

import datepicker.exception.DatepickerNotFoundException;
import datepicker.exception.DatepickerNotSavedOrUpdatedException;
import datepicker.model.DateCandidates;
import datepicker.model.Datepicker;
import datepicker.repositories.DatepickerRepository;
import datepicker.service.DatabaseOperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Component
@RestController
@RequestMapping("/api/datepicker")
public class DatepickerController {

  @Autowired private DatepickerRepository repo;

  @Autowired private DatabaseOperationService saveService;

  @GetMapping
  public List<Datepicker> datepickers(@RequestHeader("groupID") String groupID) {
    try {
      log.info("Getting all datepickers");
      return repo.getAllByGroupID(groupID);
    } catch (Exception e) {
      throw new DatepickerNotFoundException("Datepicker collection could not be found");
    }
  }

  @GetMapping(value = "{id}")
  public ResponseEntity<Datepicker> datepicker(
      @PathVariable Long id, @RequestHeader("groupID") String groupID) {
    try {
      log.info("Get datepicker by id {}", id);
      Datepicker datepicker = repo.getDatepickerByGroupIDAndDatepickerID(groupID, id);
      HttpStatus status = datepicker != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
      return new ResponseEntity<>(datepicker, status);
    } catch (Exception e) {
      throw new DatepickerNotFoundException("Datepicker with id " + id + " was not found");
    }
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Datepicker createNewDatepicker(
      @RequestBody Datepicker datepicker, @RequestHeader("groupID") String groupID) {
    try {
      log.info("Posting datepicker {}", datepicker.toString());
      return saveService.saveDatepicker(datepicker, groupID);
    } catch (Exception e) {
      throw new DatepickerNotSavedOrUpdatedException(
          "Datepicker " + datepicker.getName() + " can not be saved");
    }
  }

  @PutMapping
  public Datepicker updateDatepicker(
      @RequestBody Datepicker datepicker, @RequestHeader("groupID") String groupID) {
    try {
      log.info("Updating datepicker with id {}", datepicker.toString());
      return saveService.updateDatepicker(datepicker, groupID);
    } catch (Exception e) {
      throw new DatepickerNotSavedOrUpdatedException(
          "Updating datepicker " + datepicker.getName() + " was not allowed!");
    }
  }

  @PutMapping(value = "/cancel/{id}")
  public Datepicker cancelDatepicker(
      @PathVariable Long id, @RequestHeader("groupID") String groupID) {
    try {
      log.debug("Cancelling datepicker with id {}", id);
      return saveService.cancelDatepicker(id, groupID);
    } catch (Exception e) {
      throw new DatepickerNotSavedOrUpdatedException(
          "Cancelling datepicker " + id + " was not allowed!");
    }
  }

  @PutMapping(value = "/pick/{id}")
  public Datepicker cancelDatepicker(
      @PathVariable Long id,
      @RequestBody DateCandidates dateCandidate,
      @RequestHeader("groupID") String groupID) {
    try {
      log.debug("Picking date for datepicker with id {}", id);
      return saveService.pickDate(id, dateCandidate, groupID);
    } catch (Exception e) {
      throw new DatepickerNotSavedOrUpdatedException(
          "Picking date for datepicker " + id + " was not allowed!");
    }
  }

  @DeleteMapping(value = "{id}")
  public void delete(@PathVariable Long id) {
    try {
      log.info("Deleting datepicker with id {}", id);
      repo.deleteById(id);
    } catch (Exception e) {
      throw new DatepickerNotFoundException(
          "Datepicker with id " + id + " was not found and can not be deleted");
    }
  }
}

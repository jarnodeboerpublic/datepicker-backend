package datepicker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatepickerBackendApplication {

  public static void main(String[] args) {
    SpringApplication.run(DatepickerBackendApplication.class, args);
  }
}

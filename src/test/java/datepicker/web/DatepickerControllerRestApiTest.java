package datepicker.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import datepicker.model.DateCandidates;
import datepicker.model.Datepicker;
import datepicker.model.Reaction;
import datepicker.repositories.DatepickerRepository;
import datepicker.service.DatabaseOperationService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(value = DatepickerController.class)
class DatepickerControllerRestApiTest {

  private static final String ASSERT_CORRECT_DATEPICKER_JSON_STRING =
      "{datepickerID: 1, name: Jarno}";
  private static final String WRONG_INPUT_JSON_DATEPICKER_STRING = "wrongInput";
  private static final String INVALID_USER_ID = "100";
  private static final String RUNTIME_EXCEPTION_MESSAGE = "Ooops";
  private static final String ASSERT_GROUPID_WAS_SET =
      "{datepickerID: 1, name: Jarno, groupID: Hash}";

  @Autowired private MockMvc mockMvc;

  @MockBean private DatepickerRepository repo;

  @MockBean private DatabaseOperationService saveService;

  private Datepicker datepicker1;

  ObjectMapper mapper = new ObjectMapper();

  @BeforeEach
  void setUp() {
    mockDatepicker();
  }

  @Test
  void datepickers() throws Exception {
    List<Datepicker> list = new ArrayList<>();
    list.add(datepicker1);
    list.add(datepicker1);
    when(repo.getAllByGroupID("Hash")).thenReturn(list);

    RequestBuilder requestBuilder = requestBuilderGetbyID("/api/datepicker");
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();
    Datepicker[] datepickersArray =
        mapper.readValue(result.getResponse().getContentAsString(), Datepicker[].class);

    assertEquals(datepickersArray.length, 2);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  void noDatepickersFound() throws Exception {
    List<Datepicker> list = new ArrayList<>();
    when(repo.getAllByGroupID("Hash")).thenReturn(list);

    RequestBuilder requestBuilder = requestBuilderGetbyID("/api/datepicker");
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();

    JSONAssert.assertEquals("[]", result.getResponse().getContentAsString(), false);
  }

  @Test
  void datepickersException() throws Exception {
    doThrow(new RuntimeException(RUNTIME_EXCEPTION_MESSAGE)).when(repo).getAllByGroupID("Hash");
    RequestBuilder requestBuilder = requestBuilderGetbyID("/api/datepicker");

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
  }

  @Test
  void datepicker() throws Exception {
    when(repo.getDatepickerByGroupIDAndDatepickerID(anyString(), anyLong()))
        .thenReturn(datepicker1);

    RequestBuilder requestBuilder = requestBuilderGetbyID("/api/datepicker/1");
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    JSONAssert.assertEquals(
        ASSERT_CORRECT_DATEPICKER_JSON_STRING, result.getResponse().getContentAsString(), false);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  void datepickerException() throws Exception {
    doThrow(new RuntimeException(RUNTIME_EXCEPTION_MESSAGE))
        .when(repo)
        .getDatepickerByGroupIDAndDatepickerID(anyString(), anyLong());
    RequestBuilder requestBuilder = requestBuilderGetbyID("/api/datepicker/1");
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
  }

  @Test
  void invalidGetRequest() throws Exception {
    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.get("/api").accept(MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
  }

  @Test
  void datepickerNotFound() throws Exception {
    when(repo.findById(1L)).thenReturn(Optional.of(datepicker1));

    RequestBuilder requestBuilder = requestBuilderGetbyID("/api/datepicker/" + INVALID_USER_ID);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
  }

  @Test
  void createNewDatepicker() throws Exception {
    given(saveService.saveDatepicker(any(Datepicker.class), anyString()))
        .willAnswer(
            invocationOnMock -> {
              Datepicker datepicker = invocationOnMock.getArgument(0);
              datepicker.setGroupID(invocationOnMock.getArgument(1));

              return datepicker;
            });

    RequestBuilder requestBuilder =
        requestBuilderPost("/api/datepicker", mockDatepickerAsJsonString());

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    JSONAssert.assertEquals(
        ASSERT_CORRECT_DATEPICKER_JSON_STRING, result.getResponse().getContentAsString(), false);
    assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    JSONAssert.assertEquals(
        ASSERT_GROUPID_WAS_SET, result.getResponse().getContentAsString(), false);
  }

  @Test
  void createNewDatepickerException() throws Exception {
    doThrow(new RuntimeException(RUNTIME_EXCEPTION_MESSAGE))
        .when(saveService)
        .saveDatepicker(any(Datepicker.class), anyString());

    RequestBuilder requestBuilder =
        requestBuilderPost("/api/datepicker", mockDatepickerAsJsonString());

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
  }

  @Test
  void badRequestOnSaveDatepicker() throws Exception {
    RequestBuilder requestBuilder =
        requestBuilderPost("/api/datepicker", WRONG_INPUT_JSON_DATEPICKER_STRING);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
  }

  @Test
  void updateDatepicker() throws Exception {
    when(saveService.updateDatepicker(any(Datepicker.class), anyString())).thenReturn(datepicker1);

    RequestBuilder requestBuilder =
        requestBuilderPut("/api/datepicker", mockDatepickerAsJsonString());

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    JSONAssert.assertEquals(
        ASSERT_CORRECT_DATEPICKER_JSON_STRING, result.getResponse().getContentAsString(), false);
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  @Test
  void updateDatepickerException() throws Exception {
    doThrow(new RuntimeException(RUNTIME_EXCEPTION_MESSAGE))
        .when(saveService)
        .updateDatepicker(any(Datepicker.class), anyString());

    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.put("/api/datepicker")
            .accept(MediaType.APPLICATION_JSON)
            .content(mockDatepickerAsJsonStringWithGroupID())
            .contentType(MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
  }

  @Test
  void badRequestOnUpdateDatepicker() throws Exception {
    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.put("/api/datepicker")
            .accept(MediaType.APPLICATION_JSON)
            .content(WRONG_INPUT_JSON_DATEPICKER_STRING)
            .contentType(MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
  }

  @Test
  void delete() throws Exception {
    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.delete("/api/datepicker/1").accept(MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.OK.value(), response.getStatus());
    verify(repo).deleteById(anyLong());
  }

  @Test
  void userNotFoundExceptionDelete() throws Exception {
    willThrow(new RuntimeException(RUNTIME_EXCEPTION_MESSAGE)).given(repo).deleteById(anyLong());
    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.delete("/api/datepicker/" + INVALID_USER_ID)
            .accept(MediaType.APPLICATION_JSON);
    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
  }

  public String mockDatepickerAsJsonString() throws Exception {
    return mapper.writeValueAsString(mockDatepicker());
  }

  public String mockDatepickerAsJsonStringWithGroupID() throws Exception {
    Datepicker datepicker = mockDatepicker();
    return mapper.writeValueAsString(datepicker);
  }

  public Datepicker mockDatepicker() {
    DateCandidates candidate = new DateCandidates();
    Reaction reaction = new Reaction();
    Set<DateCandidates> candidates = new HashSet<>();
    Set<Reaction> reactions = new HashSet<>();

    datepicker1 = new Datepicker();

    candidate.setYear("2020");
    candidate.setMonth("Jan");
    candidate.setWeek("1");
    candidate.setDay("ma");
    candidate.setReactions(reactions);
    candidates.add(candidate);

    reaction.setRefusedDate(false);
    reaction.setMemberID("hash#");
    reactions.add(reaction);

    datepicker1.setName("Jarno");
    datepicker1.setDatepickerID(1L);
    datepicker1.setNewDatepicker(true);
    datepicker1.setDateCandidates(candidates);
    datepicker1.setGroupID("Hash");

    return datepicker1;
  }

  public RequestBuilder requestBuilderGetbyID(String url) {
    return MockMvcRequestBuilders.get(url)
        .accept(MediaType.APPLICATION_JSON)
        .header("groupID", "Hash");
  }

  public RequestBuilder requestBuilderPost(String url, String mockDataString) {
    return MockMvcRequestBuilders.post(url)
        .accept(MediaType.APPLICATION_JSON)
        .content(mockDataString)
        .contentType(MediaType.APPLICATION_JSON)
        .header("groupID", "Hash");
  }

  public RequestBuilder requestBuilderPut(String url, String mockDataString) {
    return MockMvcRequestBuilders.put(url)
        .accept(MediaType.APPLICATION_JSON)
        .content(mockDataString)
        .contentType(MediaType.APPLICATION_JSON)
        .header("groupID", "Hash");
  }
}

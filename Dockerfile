FROM maven:3.6.0-jdk-12-alpine AS build
RUN mkdir -p /opt/app
COPY ./ /opt/app
RUN mvn -f /opt/app/pom.xml clean package -Dmaven.test.skip=true -B

FROM adoptopenjdk/openjdk11:jre-11.0.7_10-alpine
RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY --from=build /opt/app/target/datepicker-backend.jar .
COPY --from=build /opt/app/target/dd-java-agent.jar .

ENTRYPOINT java -javaagent:dd-java-agent.jar -Ddd.trace.enabled=true -Ddd.service.name=datepicker -Ddd.integration.spring-web.enabled=true -Dspring.profiles.active=prod -jar datepicker-backend.jar
EXPOSE 8080